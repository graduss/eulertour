#include <vector>
#include <iostream>

#ifndef GRAPHS_H
#define GRAPHS_H

namespace GRAPHS {

class Graphs
{
    public:
        virtual Graphs &addEdje(unsigned n1, unsigned n2) = 0;
        virtual Graphs &removeEdje(unsigned n1, unsigned n2) = 0;
        virtual void output() const = 0;
        virtual bool isAdjacent(unsigned n1, unsigned n2) const = 0;
        virtual unsigned getNumVertices() const = 0;
        virtual std::vector<unsigned> getAdjacent(unsigned n) const = 0;
};

}
#endif // GRAPHS_H
