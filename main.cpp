#include <iostream>
#include <cstdlib>
#include <string>
#include <vector>
#include <stack>
#include <fstream>
#include "IncidenceMatrix.h"

using namespace GRAPHS;

class MarkEdges {
    public:
        MarkEdges(): matrix(NULL), n(0) {}
        MarkEdges(int num){
            matrix = new bool *[num];
            for(int i = 0; i<num; i++){
                matrix[i] = new bool [num];
                for(int j = 0; j<num; j++){
                    matrix[i][j] = false;
                }
            }
            n = num;
        }
        ~MarkEdges(){
            if(matrix){
                for(int i = 0; i<n; i++){
                    delete [] matrix[i];
                }
                delete [] matrix;
            }
            matrix = NULL;
            n = 0;
        }

        bool &edge(int i, int j){
            return matrix[i][j];
        }
        bool get(int i, int j){
            return matrix[i][j] || matrix[j][i];
        }
        void set(int i, int j, bool b){
            matrix[i][j] = matrix[j][i] = b;
        }
    private:
        bool **matrix;
        int n;
};

std::vector<std::vector<bool> > &readFile(char *fileName){
    std::ifstream file(fileName);
    if(!file) std::cerr<<'Error reading file';

    std::vector<std::vector<bool> > *matrix = new std::vector<std::vector<bool> >;
    std::string line;
    while( std::getline(file,line) ){
        if(line[0] == '+') continue;
        else{
            std::vector<bool> *l = new std::vector<bool>;
            for(int i = 1; i<line.size(); i++){
                if(line[i] == '0') l->push_back(false);
                else if (line[i] == '1') l->push_back(true);
            }
            matrix->push_back(*l);
        }
    }

    return *matrix;
}

bool isEuler(const IncidenceMatrix &g){
    unsigned n = g.getNumVertices();
    bool answer = true;

    for(unsigned i = 0; i<n; i++){
        if(g.getAdjacent(i).size()%2 != 0){
            answer = false;
            break;
        }
    }
    return answer;
}

std::vector<unsigned> &getEulerTour(const IncidenceMatrix &g, const unsigned n){
    std::vector<unsigned> *answer = new std::vector<unsigned>;

    if(!isEuler(g)) {
        std::cerr<<"The graf is not Euler";
        return *answer;
    }

    std::stack<unsigned> *temp = new std::stack<unsigned>;

    temp->push(n);
    unsigned v1, v2;
    MarkEdges markEdge = MarkEdges(g.getNumVertices());

    while(!temp->empty()){
        v1 = temp->top();
        std::vector<unsigned> adj = g.getAdjacent(v1);
        bool flag = false;

        for(int i = 0; i<adj.size(); i++){
            if(markEdge.get(v1,adj[i]) == false){
                v2 = adj[i];
                flag = true;
                break;
            }
        }

        if(flag){
            markEdge.set(v1,v2, true);
            temp->push(v2);
        }else{
            answer->push_back(v1);
            temp->pop();
        }
    }

    return *answer;
}

int main() {
    char *fileName = "data1.txt";

    std::vector<std::vector<bool> > matrix = readFile(fileName);
    IncidenceMatrix g(matrix);
    g.output();
    std::vector<unsigned> eulerTour = getEulerTour(g, 3);
    for(int i = 0; eulerTour.size() && i<eulerTour.size()-1; i++){
        std::cout<<eulerTour[i]<<"->"<<eulerTour[i+1]<<"\n";
    }
    return 0;
}
