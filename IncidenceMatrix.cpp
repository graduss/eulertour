#include "IncidenceMatrix.h"

namespace GRAPHS{

unsigned IncidenceMatrix::getNumVertices() const{
    return num_vertices;
}

 std::vector<unsigned> IncidenceMatrix::getAdjacent(unsigned n) const{
    std::vector<unsigned> answer;
    unsigned num_edje = matrix[n].size();
    for(unsigned i = 0; i<num_edje; i++){
        if(matrix[n][i]){
            for(unsigned q = 0; q<num_vertices; q++){
                if(n == q) continue;
                if(matrix[q][i]) answer.push_back(q);
            }
        }
    }

    return answer;
 }

bool IncidenceMatrix::isAdjacent(unsigned n1, unsigned n2) const {
    if(n1>=num_vertices || n2>=num_vertices) throw "for a range of values";

    unsigned num_edje = matrix[n1].size();
    for(unsigned i = 0; i<num_edje; i++){
        if(matrix[n1][i] && matrix[n2][i]) return true;
    }
    return false;
}

void IncidenceMatrix::output() const {
    if(matrix){
        std::cout<<"*** num vertices = "<<num_vertices<<" ***\n";
        for(unsigned i=0; i<num_vertices; i++){
            std::cout<<i<<" - ";
            unsigned num_edje = matrix[i].size();
            for(unsigned q=0; q<num_edje; q++){
                if(matrix[i][q]) {
                    for(unsigned w = 0; w<num_vertices; w++){
                        if(w == i) continue;
                        if(matrix[w][q]) std::cout<<w<<",";
                    }
                }
            }
            std::cout<<"\n";
        }
        std::cout<<"***************\n";
    }else std::cout<<"*** empty ***\n";
}

IncidenceMatrix &IncidenceMatrix::addEdje(unsigned n1, unsigned n2){
    if(!isAdjacent(n1,n2)){
        for(unsigned i = 0; i<num_vertices; i++){
            if(i == n1 || i == n2) matrix[i].push_back(true);
            else matrix[i].push_back(false);
        }
    }
    return *this;
}

IncidenceMatrix &IncidenceMatrix::removeEdje(unsigned n1, unsigned n2){
    unsigned num_adje = matrix[n1].size();
    for(unsigned i = 0; i<num_adje; i++){
        if(matrix[n1][i] && matrix[n2][i]){
            for(unsigned q = 0; q<num_vertices; q++){
                matrix[q].erase(matrix[q].begin() + i);
            }
        }
    }
    return *this;
}

IncidenceMatrix::IncidenceMatrix(unsigned n) {
    matrix = new std::vector<bool>[n];
    if(matrix){
        num_vertices = n;
    }else throw "Mem ERROR";
}

IncidenceMatrix::IncidenceMatrix(std::vector<std::vector<bool> > &m) {
    unsigned n = m.size();
    matrix = new std::vector<bool>[n];
    if(matrix){
        num_vertices = n;
    }else throw "Mem ERROR";

    for(unsigned i = 0; i<n; i++){
        matrix[i] = m[i];
    }
}

IncidenceMatrix::~IncidenceMatrix() {
    if(matrix){
        delete [] matrix;
        matrix = NULL;
    }
    num_vertices = 0;
}

}
